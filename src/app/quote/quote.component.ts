import { Component, OnInit } from '@angular/core';
import { QuotelistService } from '../services/quotelist.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {

  data: any = [];
  authorsName: any = [];
  constructor(private _QuotelistService: QuotelistService) { }

  ngOnInit() {

    this._QuotelistService.getAllQuotes().subscribe(res => {
      console.log(res);

      this.data = res;
      for (let i = 0; i < this.data.length; i++) {
        this.authorsName.push(this.data[i].author.name);
      }

    });
    // console.log('data', this.data);

   // this.authorsName = this.data;
   // console.log(this.authorsName);
    this.getAuthors();
  }

  getAuthors() {


  }

}
