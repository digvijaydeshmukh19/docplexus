import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { QuoteComponent } from './quote/quote.component';

import { RouterModule } from '@angular/router';
import { QuotelistService } from './services/quotelist.service';

import { HttpClientModule } from '@angular/common/http';
import { NavComponent } from './common/nav/nav.component';
import { SliderComponent } from './common/slider/slider.component';
import { QuoteCardComponent } from './quote-card/quote-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    QuoteComponent,
    NavComponent,
    SliderComponent,
    QuoteCardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path : '', redirectTo: 'home', pathMatch: 'full' },
      { path : 'home' , component: QuoteComponent}
    ])
  ],
  providers: [QuotelistService],
  bootstrap: [AppComponent]
})
export class AppModule { }
