import { TestBed, inject } from '@angular/core/testing';

import { QuotelistService } from './quotelist.service';

describe('QuotelistService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuotelistService]
    });
  });

  it('should be created', inject([QuotelistService], (service: QuotelistService) => {
    expect(service).toBeTruthy();
  }));
});
