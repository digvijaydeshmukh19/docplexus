import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class QuotelistService {

  url = './assets/data/quotes.json';
  constructor(private http: HttpClient) {

   }
   getAllQuotes() {
    return this.http.get(this.url);
   }
}
